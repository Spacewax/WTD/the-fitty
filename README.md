# The Fitty

is a website built with HTML5.

The project used this [Youtube Tutorial](https://www.youtube.com/watch?v=2R-qmsvtSaQ) as a base to begin with.

**The Fitty** is setup as a fictive company called *"The Gym"*, thus making this a nice start for any type of service oriented buisness site.


## Features

 * Responsive HTML5/CSS3
 * Image Slider
 * Drop Down Menu


### License

Undecided yet. MIT, BSD, AGPL or GPL